# Development guide for GitLab CI/CD templates

## Template directories

| Sub-directory  | Template type                                       |
| -------------- | --------------------------------------------------- |
| `/*` (root)    | General templates.                                  |
| `/Java/*`      | Actions related to Java.                            |
| `/Kotlin/*`    | Actions related to Kotlin.                          |
| `/Docker/*`    | Actions related to Docker.                          |
| `/.../*`       | Actions related to ...                              |
| `/Workflows/*` | Sample templates for using the `workflow:` keyword. |
