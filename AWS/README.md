# Shell actions for AWS

CI/CD Pipeline for different projects in most of the case will have multiple similar component in its project pipeline (like clean, build, install, then publish for application pipeline or init, plan, validate and apply for infra pipeline), there are a lot of duplicate code for the pipeline with different code style and may include defected script. With “Shared Gitlab actions” we want to keep the CI/CD declarative Pipeline DRY, and keep up with high standard in the entire organization.

# List of Common Actions

| Action Groups | Action Name                | Params required              |
| :------------ | :------------------------- | :--------------------------- |
| AWS Actions   | assume_role_wo_external_id | ASSUME_ROLE_ARN              |
|               | assume_role_w_external_id  | ASSUME_ROLE_ARN, EXTERNAL_ID |
|               | remove_assume_role         |                              |
|               | ecr_login                  | ECR_REGISTRY_URL             |
