# Gitlab CI template

## List of Group Actions

CI/CD Pipeline for different projects in most of the case will have multiple similar component in its project pipeline (like clean, build, install, then publish for application pipeline or init, plan, validate and apply for infra pipeline), there are a lot of duplicate code for the pipeline with different code style and may include defected script. With “Shared Gitlab actions” we want to keep the CI/CD declarative Pipeline DRY, and keep up with high standard in the entire organization.
Shared Gitlab Actions is a set of reusable script for Gitlab CI .gitlab-ci.yml file, every DevOps/Dev can contribute to this Shared Actions for other teams to use, code quality must to approved by Senior DevOps/TechLead. Actions is separated into a specific domain group like Actions relating to AWS, Docker, Terraform or Maven,…

| Group actions | Lcation                                              |
| :------------ | :--------------------------------------------------- |
| AWS           | [AWS/Base.gitlab.yml](AWS/Base.gitlab.yml)           |
| Java          | [Java/Base.mvn.gitlab.yml](Java/Base.mvn.gitlab.yml) |
| Helm          | [Helm/Base.gitlab.yml](Helm/Base.gitlab.yml)         |
| Git           | [Git/Base.gitlab.yml](Git/Base.gitlab.yml)           |
| Docker        | [Git/Base.gitlab.yml](Docker/Base.gitlab.yml)        |

## Gitlab Pipeline Templates

To further extend the capability of DevOps team, Gitlab Pipeline Templates is introduced. These templates are the pre-written stages and steps for commonly used tech stack. The templates is written in high-quality enough for teams with less experience with ci/cd pipeline can re-use in their projects with highly customizable features like the version of the tools, turn on/off features included in that templates,….

The Pipeline Templates is an extended version of the Shared Gitlab Actions. The pre-made template is for popular tech stack inside org such as Pipeline Templates for Java Maven, Gradle, Terraform,… Every templates has it own version control, and support various ad-hoc scenario with support different input variables matches the project team needs.

| Templates Name  | Tech stack              | Location                                       |
| :-------------- | :---------------------- | :--------------------------------------------- |
| Docker pipeline | Docker dind, Docker cli | [exmple](example_template/docker_pipeline.yml) |
| Helm pipeline   | Helm, Git               | [exmple](example_template/helm_pipeline.yml)   |

# Development guide for GitLab CI/CD templates

This [document](doc/guide-develop.md) explains how to develop GitLab CI/CD templates.
